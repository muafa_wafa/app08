package muafa.sirojul.app06b

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db: SQLiteDatabase
    lateinit var fragProdi: FragmentProdi
    lateinit var fragMhs: FragmentMahasiswa
    lateinit var ft: FragmentTransaction
    lateinit var fragkali: Fragmentkali

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)

        this.bottomNavigationView.setOnNavigationItemSelectedListener(this)
        this.fragProdi = FragmentProdi()
        this.fragMhs = FragmentMahasiswa()
        this.fragkali = Fragmentkali()
        this.db = DBOpenHelper(this).writableDatabase

    }

    fun getDbObject(): SQLiteDatabase {
        return this.db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemProdi -> {
                this.ft = this.supportFragmentManager.beginTransaction()
                this.ft.replace(R.id.frameLayout, this.fragProdi).commit()
                this.frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                this.frameLayout.visibility = View.VISIBLE

            }
            R.id.itemMhs -> {
                this.ft = this.supportFragmentManager.beginTransaction()
                this.ft.replace(R.id.frameLayout, this.fragMhs).commit()
                this.frameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                this.frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout ->
                this.frameLayout.visibility = View.GONE


            R.id.itemkali ->{
                this.ft = this.supportFragmentManager.beginTransaction()
                this.ft.replace(R.id.frameLayout, this.fragkali).commit()
                this.frameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                this.frameLayout.visibility = View.VISIBLE

                true
            }

            R.id.itemsitus ->{
                var weburi = "https://polinema.ac.id"
                var intentinternet = Intent(Intent.ACTION_VIEW, Uri.parse(weburi))
                this.startActivity(intentinternet)
            }

        }
        return true
    }
}